const fs = require('fs');

function escribir(datos) {
    let date = new Date();
    fs.writeFile(('./datos/' + date.getFullYear() + date.getMonth() + date.getDay() + date.getHours() + date.getMinutes() + date.getSeconds() + '.json'), JSON.stringify(datos), function (err) {
        if (err) throw err;
    });
}

const getFile = nombreArchivo => {
    return new Promise((resolve, reject) => {
        fs.readFile(nombreArchivo, (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data);
        })
    });
}

function leer(nombre) {
    getFile('./' + nombre + '.json')
        .then(JSON.parse)
        .then(console.log)
        .then(console.error);
}

module.exports =
    {
        leer: leer,
        escribir: escribir
    }