require('log-timestamp');
var leerDatos = require('./datos');
var archivos = require('./archivos');
const fs = require('fs');
const readLine = require('readline');
var lector = readLine.createInterface(
    {
        input: process.stdin,
        output: process.stdout
    }
);

function actualizarDatos() {
    leerDatos().then(datos => {
        archivos.escribir(datos);
    });
    menu();
};

function calcularPromedio() {
    fs.readdir('./datos', function (err, archivos) {
        if (err) return console.error('No se pudo encontrar los archivos.');
    });

    menu();
};

function minimoYMaximo() {
    let mayor = 0;
    let menor = 0;
    fs.readdir('./datos', {withFileTypes:false} ,function (err, archivos) {
        if (err) return console.error('No se pudo encontrar los archivos.');
        if (archivos[0] == '.DS_Store') archivos.splice(0,1);
        archivos.forEach(function (archivo) {
            archivo = archivo.replace('.json', '')
            console.log(archivo);
        });
    });
    menu();
};

function menu() {
    console.log('MENU');
    console.log('1. Actualizar datos');
    console.log('2. Promedio últimos 5 archivos');
    console.log('3. Mínimo y máximo últimos 5 archivos');
    console.log('4. Salir');
    lector.question('Escriba su opción\n', opcion => {
        switch (opcion) {
            case '1': actualizarDatos(); break;
            case '2': calcularPromedio(); break;
            case '3': minimoYMaximo(); break;
            case '4':
                lector.close();
                process.exit(0);
                break;
            default: menu();
                break;
        }
    });
};

menu();